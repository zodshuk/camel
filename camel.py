"""
The Camel Static Site Generator. A powerful tool for building
Single-Page Web Applications utilising hash routing. In 100 LOC.
                  ,,__
        ..  ..   / o._)                   .---.
       /--'/--\  \-'||        .----.    .'     '.
      /        \_/ / |      .'      '..'         '-.
    .'\  \__\  __.'.'     .'          i-._
      )\ |  )\ |      _.'
     // \\ // \\
    ||_  \\|_  \\_
mrf '--' '--'' '--'
"""
JS_TEMPLATE = """const root=document.getElementById("root");""" + \
    """$$$;function renderElement(e){if("text"===e[0])return""" + \
    """ document.createTextNode(e[1]);let t=document.createE""" + \
    """lement(e[0]);for(let o of e[2])t.appendChild(renderEl""" + \
    """ement(o));for(let o of e[1])t.setAttribute(o[0],o[1])""" + \
    """;return t}function renderRoute(){let e=document.locat""" + \
    """ion.hash.split("#")[1]||"/",t=site[e]||site["/error40""" + \
    """4"];for(let e of root.children)root.removeChild(e);ro""" + \
    """ot.appendChild(renderElement(t))}window.addEventListe""" + \
    """ner("hashchange",renderRoute),window.addEventListener""" + \
    """("load",renderRoute);"""

class Map(dict):
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__

def makeElement(tag, children):
    def atr(name, value):
        element.attributes.append([name, value])
        return element

    def style(atr, value):
        style_atr = list(filter(
            lambda a: a[0] == "style", element.attributes))
        if style_atr:
            for i, atr_ in enumerate(element.attributes):
                if atr_[0] == "style":
                    element.attributes[i][1] += " {}: {};".format(
                        atr, value
                    )
        else:
            element.attributes.append(["style", "{}: {};".format(
                atr, value
            )])
        return element

    def cls(cls):
        class_atr = list(filter(
            lambda c: c[0] == "class", element.attributes))
        if class_atr:
            for i, cls_ in enumerate(element.attributes):
                if cls_[0] == "class":
                    element.attributes[i][1] += " " + cls
        else:
            element.attributes.append(["class", cls])
        return element

    element = Map({
        "tag": tag,
        "attributes": [],
        "children": children,
        "atr": atr,
        "style": style,
        "cls": cls
    })

    return element

def div(*children): return makeElement("div", children)
def p(*children):   return makeElement("p", children)
def h1(*children):  return makeElement("h1", children)
def h2(*children):  return makeElement("h2", children)
def h3(*children):  return makeElement("h3", children)
def img(*children): return makeElement("img", children)
def a(*children):   return makeElement("a", children)
def br(*children):  return makeElement("br", children)
def hr(*children):  return makeElement("hr", children)
def ul(*children):  return makeElement("ul", children)
def li(*children):  return makeElement("li", children)
def span(*children):  return makeElement("span", children)

def _makeJSElem(elem):
    if isinstance(elem, str):
        return ["text", elem]

    return [
        elem.tag,
        elem.attributes,
        [_makeJSElem(el) for el in elem.children]
    ]

def _makeJSRouter(router):
    JSRouter = {}

    for key in router.keys():
        if isinstance(router[key], Map):
            JSRouter[key] = _makeJSElem(router[key])

    return JSRouter

def makeRouter():
    def route(name, document):
        router[name] = document

    def generate():
        js_doc = _makeJSRouter(router)
        
        _template = JS_TEMPLATE.replace(
                "$$$", "const site = " + str(js_doc))

        with open("./camel.js", "w") as f:
            f.write(_template)

        return js_doc

    router = Map({
        "route": route,
        "generate": generate
    })

    return router
