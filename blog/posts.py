from camel import *

posts = [
    Map({
        "date": "05012024",
        "title": "On the End of the World",
        "document": div(
            p("""No! You don't even believe that! Gus has cameras everywhere, please. Listen to yourself! No, he has known everything, all along. Where were you today? In the lab? And you don't think it's possible that Tyrus lifted the cigarette out of your locker? Come on! Don't you see? You are the last piece of the puzzle. You are everything that he's wanted."""),
            p("""You're his cook now. You're the cook and you have proven that you can run a lab without me and now that cook has reason to kill me. Think about it! It's brilliant. So go ahead. If you think that I am capable of doing this, then go ahead. Put a bullet, in my head, and kill me right now. DO IT! Do it. Do it. Do it."""),
            p("""Saul, Saul... this man that we spoke of before, this... this person that you said could... could disappear me, get me a whole new life and make sure that I'm never found. Yeah I need him, Saul. Gus is gonna murder my whole family. I need this man now. Saul... now, Saul!"""),
            p("""Anything suspicious? Well... then should we go? Any uh... Cartel news these days? Seems like I'm always reading something or other in the paper. I don't want to talk about it. To you or anyone else. I'm done explaining myself. Gus is dead. We've got work to do.""")
        )
    }),
    Map({

        "date": "11022024",
        "title": "On Hell",
        "document": div(
            p("""I have been waiting. I've been waiting all day. Waiting for Gus to send one of his men to kill me. And it's you. Who do you know, who's okay with using children, Jesse? Who do you know... who's allowed children to be murdered... hmm? Gus! He has, he has been ten steps ahead of me at every turn. And now, the one thing that he needed to finally get rid of me is your consent and boy he's got that now. He's got it. And not only does he have that, but he manipulated you into pulling the trigger for him."""),
            p("""No! You don't even believe that! Gus has cameras everywhere, please. Listen to yourself! No, he has known everything, all along. Where were you today? In the lab? And you don't think it's possible that Tyrus lifted the cigarette out of your locker? Come on! Don't you see? You are the last piece of the puzzle. You are everything that he's wanted. """),
            p("""You're his cook now. You're the cook and you have proven that you can run a lab without me and now that cook has reason to kill me. Think about it! It's brilliant. So go ahead. If you think that I am capable of doing this, then go ahead. Put a bullet, in my head, and kill me right now. DO IT! Do it. Do it. Do it."""),
            p("""Saul, Saul... this man that we spoke of before, this... this person that you said could... could disappear me, get me a whole new life and make sure that I'm never found. Yeah I need him, Saul. Gus is gonna murder my whole family. I need this man now. Saul... now, Saul!"""),
            p("""Anything suspicious? Well... then should we go? Any uh... Cartel news these days? Seems like I'm always reading something or other in the paper. I don't want to talk about it. To you or anyone else. I'm done explaining myself. Gus is dead. We've got work to do."""),
            p("""Jesse Jackson? Do you even... ah, I see you have a telephone at least. You know that blinking thing I've been calling you on? I will break this, I will BREAK THIS. Damn druggie idiot. Is this what you've been doing the whole time I've been trying to reach you?"""),            
            )
    }),
    Map({
        "date": "20022024",
        "title": "An Introduction to Time Sorcery",
        "document": div(
            p("""Walter, I'm your lawyer. Anything you say to me is totally privileged. I'm not in the shakedown racket. I'm a lawyer. Even drug dealers need lawyers, right? Especially drug dealers."""),
            p("""No shit! Right now you're Fredo. But, y'know, with some sound advice and proper introductions, who knows? I'll tell you one thing: you've got the right product. Anything that gets the DEA's panties in this big a bunch, you're onto something special. And I would like to be a small and silent part of it. Food for thought, yeah? So if you want to make more money and keep the money that you make, better call Saul! """),
            p("""Better safe than sorry. That's my motto. Yeah, you do seem to have a little 'shit creek' action going. You know, FYI, you can buy a paddle. Did you not plan for this contingency? I mean the Starship Enterprise had a self-destruct button. I'm just saying. """)
        )
    }),
    Map({
        "date": "05032024",
        "title": "The Black Lodge and the Architectonic Order of the Eschaton",
        "document": div(
            p("""Look, let's start with some tough love, alright? Ready for this? Here it goes: you two suck at peddling meth. Period. Good! 'Oh boo-hoo, I won't cook meth anymore!' You're a crybaby! Who needs you?! Hey, I'm unplugging the website, so no more money laundering! How do you like that?!"""),
            p("""Great, perfect you know... this is just. I told her you were my A Team. Oh, hello Mrs. White, the good news is the IRS has been paid off, the bad news is... ach, Jesus!"""),
            p("""What am I eighth grade hall monitor? Current whereabouts? Let me tell you something, Mike. There are rules to this lawyer thing. Attorney client privilege, that's a big one. That's something I provide for you! If I give up Pinkman, then you're gonna be asking, old Saul gives 'em up pretty easy what's to keep him from giving me up? You see, so then, where's the trust?""")
        )
    })
]
