from camel import *
from posts import posts

router = makeRouter()

navbar = ul(
    *[li(a(item[0]).atr("href", item[1])) for item in [
        ["Home", "#/"],
        ["About", "#/about"],
        ["All Posts", "#/posts"],       
    ]]
).cls("navbar")

header = div(
    div(
        h1("Welcome to my Blog!"),
        navbar
    ).cls("header"),
    img().atr("src", "logo.png"),
    div().cls("clear"),
    hr()
)

latest_posts = div(
    h3("Latest Posts:"),
    ul(
        *[li(
            span(post.date).cls("post-date"),
            a(post.title).atr("href", "#/posts/" + post.date)
        ) for post in posts[-3:]]
    )
)

router.route("/", div(
    header,
    p("Built with Camel.py!").cls("camel"),
    latest_posts
))

router.route("/posts", div(
    header,
    h3("Index of Posts:"),
    ul(
        *[li(
            span(post.date).cls("post-date"),
            a(post.title).atr("href", "#/posts/" + post.date)
        ) for post in posts]
    )
))

router.route("/about", div(
    header,
    h3("About"),
    p(
        "All Lipsum via ",
        a("heisenbergipsum.com").atr("href", "https://heisenbergipsum.com/")
    ),
    p("""What? Come on! Man, you're smart. You made poison out of beans, yo. Look, we got, we got an entire lab right here. Alright? How about you pick some of these chemicals and mix up some rocket fuel? That way you could just send up a signal flare. Or you make some kind of robot to get us help, or a homing device, or build a new battery, or... wait. No. What if we just take some stuff off of the RV and build it into something completely different? You know, like a... Like a dune buggy. That way, we can just dune buggy or... What? Hey? What is it? What?""")
))

router.route("/error404", div(
    header,
    h3("Error 404: Page Not Found!").cls("ERROR")
))

for post in posts:
    router.route("/posts/" + post.date, div(
        header,
        h2(post.title),
        span(post.date).cls("post-date"),
        post.document
    ))

router.generate()
