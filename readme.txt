# The Camel Static Site Generator

The library is one file and less than 100 loc. You just need to import
`camel.py`; see demo in `blog/` for example code. Check out the files
`index.html` and the generated file `camel.js` to see how minimal Cam-
el can make your site!

N.B. You must create a `\error404' route to avoid undefined behaviour.
